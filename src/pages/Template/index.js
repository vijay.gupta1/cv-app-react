import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card } from 'technogetic-iron-smart-ui';
import { Footer } from '../../components/Footer';
import { Loader } from '../../components/Loader';
import { dataContext } from '../../context/dataContext';
import styles from './styles.module.css';

export const Template = () => {
  const { user, data, setActiveTemplate } = useContext(dataContext);

  const handleTemplate = (i) => {
    setActiveTemplate(i);
  };
  

  if (!user) {
    return <Loader />;
  }
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <img className={styles.logoimg} src='logoTG.png' alt='logo' />
        <Link to={'/app'}>
          <Button className={styles.btn}>Back to Home</Button>
        </Link>
      </div>
      <div className={styles.templates}>
        <h2 className={styles.template_header}>All Templates</h2>
        <section className={styles.cardSection}>
          {[1, 2, 3, 4,5,6,7].map((elm) => (
            <Link key={elm} to={'/cv'} className={styles.cards}>
              <Card onClick={() => handleTemplate(elm)} className={`${styles.card1} ${styles.card}`}  title='' footer=''>
                <p>Template {elm}</p>
               
              </Card>
            </Link>
          ))}
        </section>
      </div>
      <Footer></Footer>
    </div>
  );
};
