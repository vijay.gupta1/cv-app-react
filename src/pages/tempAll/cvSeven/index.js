import React, { useContext } from 'react';
import { Loader } from '../../../components/Loader';
import { Tempseven } from '../../../components/templateSeven';
import { dataContext } from '../../../context/dataContext';

export default function Cvseven() {
  const { data } = useContext(dataContext);

  if (!data) {
    return <Loader />;
  }
  return (
    <div>
      <div className='app'>
        <Tempseven />
      </div>
    </div>
  );
}
