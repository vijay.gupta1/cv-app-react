import React, { useContext } from 'react';
import { Loader } from '../../../components/Loader';
import { Tempsix } from '../../../components/templateSix';
import { dataContext } from '../../../context/dataContext';
import styles from './styles.module.css';
import { Header } from '../../../components/templateSix/Header';
import { Title } from '../../../components/templateSix/Title';

export const  Cvsix = ({ summary }) =>  {
  const { data } = useContext(dataContext);

  if (!data) {
    return <Loader />;
  }
  return (
    <div>
      <div className={styles.app}>
      <Header {...data} />
         <div className='details-summary'>
              <Title title={'Summary'} />
              <p contentEditable='true' suppressContentEditableWarning={true} className='textDark '>
                {data.summary}
              </p>
              <br/>
              <br/>
            </div>
        
        <Tempsix />
      </div>
    </div>
  );
}
