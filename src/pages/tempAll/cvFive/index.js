import React, { useContext } from 'react';
import { Loader } from '../../../components/Loader';
import { Tempfive } from '../../../components/templateFive';
import { dataContext } from '../../../context/dataContext';

export default function Cvfive() {
  const { data } = useContext(dataContext);

  if (!data) {
    return <Loader />;
  }
  return (
    <div className="app">
      <Tempfive/>
    </div>
  );
}
