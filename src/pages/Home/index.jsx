import { useContext, useEffect, useState } from 'react';
import styles from './styles.module.css';
import jwt_decode from 'jwt-decode';
import { Loader } from '../../components/Loader';
import { dataContext } from '../../context/dataContext';
import { BASE_URL } from '../../constant';
import Resumes from '../../components/Resumes';
import { Footer } from '../../components/Footer';
import { Button } from 'technogetic-iron-smart-ui';

function Home() {
  const { user, setUser } = useContext(dataContext);
  const [display, setDisplay] = useState(false);

  const handleDisplay = () => {
    setDisplay(false);
  };

  const logout = () => {
    window.open(`${BASE_URL}auth/logout`, '_self');
    localStorage.clear();
  };

  useEffect(() => {
    setUser(jwt_decode(localStorage.getItem('token')).user);
  }, []);

  if (!user) {
    return <Loader />;
  }
  return (
    <>
      <div className={styles.container}>
        <header className={styles.header} id='appbar'>
          <img className={styles.logoimg} src='logoTG.png' alt='logo' />
          <div className={styles.profile}>
            <div className='tooltip'>
              <img
                onClick={() => setDisplay(!display)}
                src={user.photo ? user.photo : '🙂'}
                alt='profile'
                className={styles.profile_img}
              />
              {display && (
                <div className='tooltiptext'>
                  {/* <h2>{user.email}</h2> */}
                  <h2 className={styles.from_heading}>{user.name}</h2>
                  <Button className={styles.btn} onClick={logout}>
                    Log Out
                  </Button>
                </div>
              )}
            </div>
          </div>
        </header>
        <main onClick={handleDisplay}>
          <Resumes />
          <Footer></Footer>
        </main>
      </div>
    </>
  );
}

export default Home;
