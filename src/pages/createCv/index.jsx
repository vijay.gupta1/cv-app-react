// import CustomizedSnackbars from "../../components/Alert";
import styles from './styles.module.css';
import React, { useContext, useState } from 'react';
import { Formik, Field, Form, ErrorMessage, FieldArray } from 'formik';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { BASE_URL } from '../../constant';
import { allField, contactField, eduField, projects, workField } from '../../mock';
import { CvSchema } from '../../schema/cvSchema';
import { dataContext } from '../../context/dataContext';
import { Footer } from '../../components/Footer';
import { Button } from 'technogetic-iron-smart-ui';

export default function CreateCV() {
  const navigate = useNavigate();
  const { data, setData, user } = useContext(dataContext);
  const [submitStatus, setsubmitStatus] = useState(false);
  let url = `${BASE_URL}api/createPdf/${user._id}`;
  const config = {
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('token')
    }
  };

  // const handleUpdateCv =()=>{
  //   axios
  //     .post(`${BASE_URL}api/updatePdf/${data._id}`,data,config)
  //     .then(res => console.log(res))
  //     .catch(err => console.error(err));
  //     navigate("/home");
  // }

  return (
    <div className={styles.Updatecv}>
      <div className={styles.header}>
        <img className={styles.logoimg} src='logoTG.png' alt='logo' />
        <Link to={'/app'}>
          <Button>Home</Button>
        </Link>
      </div>
      <div className={styles.container}>
        <Formik
          initialValues={{
            ...data,
            skills: {
              technology: '',
              database: '',
              apis: '',
              realtimeDb: ''
            }
          }}
          validationSchema={CvSchema}
          onSubmit={(values) => {
            axios
              .post(url, values, config)
              .then((res) => setData(res.data) || setsubmitStatus(true))
              .catch((err) => console.error(err));
            alert('Form Submited Succesfully !');
            navigate('/app');
          }}
        >
          {({ values, setFieldValue, handleSubmit }) => (
            <div className={styles.main}>
              <div className={styles.form}>
                <Form>
                  <div >
                    {allField.map((text, i) => {
                      if (text === 'skills') {
                        return (
                          <div className={styles.border}>
                            <div className={styles.col}>
                              <label>Skills :</label>
                            </div >
                         
                            <div className={styles.row}>
                              <div>
                                <label>Frameworks :</label>
                                <div>
                                  <Field
                                    name={'technology'}
                                    value={values.skills.technology}
                                    onChange={({ target }) =>
                                      setFieldValue('skills.technology', target.value)
                                    }
                                    placeholder={'Framework and library here '}
                                    type='text'
                                  />
                                </div>
                              </div>
                              <div className={styles.form_input_container}>
                                <label>Database :</label>
                                <div>
                                  <Field
                                    name={'database'}
                                    value={values.skills.database}
                                    onChange={({ target }) =>
                                      setFieldValue('skills.database', target.value)
                                    }
                                    placeholder={'Database'}
                                    type='text'
                                  />
                                </div>
                              </div>
                              <div className={styles.form_input_container}>
                                <label>APIs :</label>
                                <div>
                                  <Field
                                    name={'apis'}
                                    value={values.skills.apis}
                                    onChange={({ target }) =>
                                      setFieldValue('skills.apis', target.value)
                                    }
                                    placeholder={'APIs'}
                                    type='text'
                                  />
                                </div>
                              </div>
                              <div className={styles.form_input_container}>
                                <label>Real Time DB :</label>
                                <div>
                                  <Field
                                    name={'realtimeDb'}
                                    value={values.skills.realtimeDb}
                                    onChange={({ target }) =>
                                      setFieldValue('skills.realtimeDb', target.value)
                                    }
                                    placeholder={'Real time DB'}
                                    type='text'
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      }

                      return (
                        <div key={i} className={styles.col}>
                          <label htmlFor={text}>{text}</label>
                          <Field
                            as={text === 'summary' ? 'textarea' : ''}
                            name={text}
                            placeholder={text}
                            type='text'
                          />
                          <ErrorMessage name={text} component='div' className='field-error' />
                        </div>
                      );
                    })}

                    <div className={styles.border}>
                      <div className={styles.col}>
                        <label>Contact Us :</label>
                      </div>
                      <FieldArray name='contactUs'>
                        {() => (
                          <div>
                            <div className={styles.row}>
                              {contactField.map((title, index) => (
                                <div className={styles.colsm} key={index}>
                                  <label htmlFor={`contactUs[0].${title}`}>{title}</label>
                                  <Field
                                    name={`contactUs[0].${title}`}
                                    placeholder={`${title}`}
                                    type='text'
                                  />
                                  <ErrorMessage
                                    name={`contactUs[0].${title}`}
                                    component='div'
                                    className='field-error'
                                  />
                                </div>
                              ))}
                            </div>
                          </div>
                        )}
                      </FieldArray>
                    </div>
{/* education */}
                    <div className={styles.border}>
                      <div className={styles.col}>
                        <label>Education :</label>
                      </div>
                      <FieldArray name='education'>
                        {({ remove, push }) => (
                          <div>
                            {values.education.length > 0 &&
                              values.education.map((obj, i) => (
                                <div className={styles.row} key={i}>
                                  

                                  {eduField.map((title, index) => (
                                    <div className={styles.colsm} key={index}>
                                      <label htmlFor={`education.${i}.${title}`}>{title}</label>
                                      <Field
                                        name={`education.${i}.${title}`}
                                        placeholder={`${title}`}
                                        type='text'
                                      />
                                      <ErrorMessage
                                        name={`education.${i}.${title}`}
                                        component='div'
                                        className='field-error'
                                      />
                                    </div>
                                  ))}
                                  <div className={styles.deleteEdu}>
                                    {i > 0 && (
                                      <Button
                                        type='button'
                                        // className='secondary'
                                        onClick={() => remove(i)}
                                      >
                                        X
                                      </Button>
                                    )}
                                  </div>
                                </div>
                              ))}

                            <div className={styles.addButton}>
                              <Button
                                type='button'
                                // className='secondary'
                                onClick={() =>
                                  push({
                                    qualification: '',
                                    fromWhere: '',
                                    when: ''
                                  })
                                }
                              >
                                Add Education
                              </Button>
                            </div>
                          </div>
                        )}
                      </FieldArray>
                    </div>
{/* work experience */}
<div className={styles.border}>
                      <div className={styles.col}>
                        <label>Work Experience :</label>
                      </div>
                      <FieldArray name='workExprience'>
                        {({ remove, push }) => (
                          <div>
                            {values.workExprience.length > 0 &&
                              values.workExprience.map((obj, i) => (
                                <div className={styles.row} key={i}>
                                 

                                  {workField.map((title, index) => (
                                    <div className={styles.colsm} key={index}>
                                      <label htmlFor={`workExprience.${i}.${title}`}>{title}</label>
                                      <Field
                                        name={`workExprience.${i}.${title}`}
                                        placeholder={`${title}`}
                                        type='text'
                                      />
                                      <ErrorMessage
                                        name={`workExprience.${i}.${title}`}
                                        component='div'
                                        className='field-error'
                                      />
                                    </div>
                                  ))}
                                   <div className={styles.deleteEdu}>
                                    {i > 0 && (
                                      <Button
                                        type='button'
                                        // className='secondary'
                                        onClick={() => remove(i)}
                                      >
                                        X
                                      </Button>
                                    )}
                                  </div>
                                </div>
                              ))}

                            <div className={styles.addButton}>
                              <Button
                                type='button'
                                // className='secondary'
                                onClick={() =>
                                  push({
                                    companyname: '',
                                    date: ''
                                  })
                                }
                              >
                                Add Work Experience
                              </Button>
                            </div>
                          </div>
                        )}
                      </FieldArray>
                    </div>

                    {/* projects */}
                    <div className={styles.border}>
                      <div className={styles.col}>
                        <label>Projects :</label>
                      </div>
                      <FieldArray name='projects'>
                        {({ remove, push }) => (
                          <div>
                            {values.projects.length > 0 &&
                              values.projects.map((obj, i) => (
                                <div className={styles.row} key={i}>
                                 

                                  {projects.map((title, index) => (
                                    <div className={styles.colsm} key={index}>
                                      <label htmlFor={`projects.${i}.${title}`}>{title}</label>
                                      <Field
                                        as={title === 'description' && 'textarea'}
                                        name={`projects.${i}.${title}`}
                                        placeholder={`${title}`}
                                        type='text'
                                      />
                                      <ErrorMessage
                                        name={`projects.${i}.${title}`}
                                        component='div'
                                        className='field-error'
                                      />
                                    </div>
                                  ))}
                                   <div className={styles.deleteEdu}>
                                    {i > 0 && (
                                      <Button
                                        type='button'
                                        className='secondary deletButton'
                                        onClick={() => remove(i)}
                                      >
                                        X
                                      </Button>
                                    )}
                                  </div>
                                </div>
                              ))}
                            <div className={styles.addButton}>
                              <Button
                                type='button'
                                // className='secondary' 
                                onClick={() =>
                                  push({
                                    projectName: '',
                                    role: '',
                                    techTools: '',
                                    teamSize: '',
                                    url: '',
                                    description: ''
                                  })
                                }
                              >
                                Add Projects
                              </Button>
                            </div>
                          </div>
                        )}
                      </FieldArray>
                    </div>
                  </div>

                  <div className={styles.bottom}>
                    <div className={styles.checkbox}>
                      <label>
                        <Field type='checkbox' name='isLogo' />
                        {`Show Logo and Footer`}
                      </label>
                    </div>{' '}
                  </div>
                  <div className={styles.submit}>
                    <Button type='submit'>
                      Submit
                      {/* <CustomizedSnackbars submitStatus={submitStatus}/> */}
                    </Button>
                  </div>
                </Form>
              </div>
            </div>
          )}
        </Formik>
      </div>
      <Footer></Footer>
    </div>
  );
}
