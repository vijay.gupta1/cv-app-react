export const rowwData = [
  {
    Name: 'Name 1',
    Email: 'Email',
    Id: 'User Id',
    Delete: `Delete`,
    View: 'View All CV'
  },
  {
    Name: 'Name 2',
    Email: 'Email',
    Id: 'User Id',
    Delete: `Delete`,
    View: 'View All CV'
  },
  {
    Name: 'Name 3',
    Email: 'Email',
    Id: 'User Id',
    Delete: `Delete`,
    View: 'View All CV'
  }
];

export const colData = [
  { field: 'Name' },
  { field: 'Email' },
  { field: 'Id' },
  { field: 'Delete' },
  { field: 'View' }
];

export const initialState = {
  isLogo: true,
  name: '',
  post: '',
  summary:
    '',
  education: [{ qualification: '', fromWhere: ' ', when: '' }],
  workExprience: [{ companyname: '', date: ' '}],
  projects: [
    {
      projectName: '',
      description:
        ' ',
      responsibility: '',
      role: ' ',
      techTools: '',
      teamSize: '',
      url: ''
    }
  ],
  skills: {
    technology: '',
    database: '',
    apis: '',
    realtimeDb: ''
  },
  tools: '',
  langaugeSkills: '',
  contactUs: [
    {
      companyName: '',
      email: '',
      mobilenumber: '',
      location: ' '
    }
  ]
};

export const allField = ['name', 'post', 'summary', 'skills', 'tools', 'langaugeSkills'];

export const contactField = ['companyName', 'email', 'mobilenumber', 'location'];

export const eduField = ['qualification', 'fromWhere', 'when'];

export const workField= [ 'companyname', 'date'];

export const projects = [
  'ProjectName',
  'role',
  'responsibility',
  'techTools',
  'teamSize',
  'url',
  'description'
];

export const config = {
  headers: {
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
};

