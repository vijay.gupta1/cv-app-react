import React from 'react';
import styles from './style.module.css';

export const Subtitle = ({ subTitle }) => {
  return (
    <div contentEditable={true} suppressContentEditableWarning={true} className={styles.subtitle}>
      {subTitle}
    </div>
  );
};
