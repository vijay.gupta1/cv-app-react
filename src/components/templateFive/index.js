import React, { useContext } from 'react';
import { dataContext } from '../../context/dataContext';
import { Header } from './Header';
import { Subtitle } from './Subtitle';
import styles from './style.module.css';

export const Tempfive = () => {
  const { data } = useContext(dataContext);

  return (
    <div className={styles.conatiner}>
      <div className={styles.heading_container}>PROFILE</div>
      <Header {...data} />

      <div><div className={styles.heading_container}>PROFILE SUMMARY</div>
      <div className={styles.content_container}><Subtitle  subTitle={data.summary}  /></div></div>

    <div>  <div className={styles.heading_container}>SKILLS & ABILITIES</div>
      {console.log(data)}
      <div className={styles.content_container}>
        {Object.keys(data.skills).map((skillName, i) => {
          if (data.skills[skillName]) {
            return (
              <li
                key={i}
                contentEditable={true}
                suppressContentEditableWarning={true}
                className={styles.subtitle}
              >
                <strong>{skillName} :</strong>
                <span>{data?.skills[skillName]}</span>
              </li>
            );
          } else {
            return null;
          }
        })}
        {data?.tools && (
          <li
            contentEditable={true}
            suppressContentEditableWarning={true}
            className={styles.subtitle}
          >
            <strong>Project Management tools :</strong>
            <span>{data.tools}</span>
          </li>
        )}
      </div></div>

      <div>{data.education[0].qualification && (
        <div>
          <div className={styles.heading_container}>Education</div>

          <div className={styles.content_container}>
            {data?.education?.map((elm, i) => (
              <div key={i} className={styles.edu}>
                <p> {elm.qualification}</p>
                <p>{elm.fromWhere}</p>
                <p>{elm.when}</p>
              </div>
            ))}
          </div>
        </div>
      )}</div>

      <div>
        <div className={styles.heading_container}>BRIEF SUMMARY OF EXPERIENCE
</div>
        <div className={styles.content_container}>
          {data?.projects?.map((elm, i) => (
            <div key={i} className={styles.project}>
              <h3>
                <strong>PROJECT {i + 1}</strong> ({elm.projectName})
              </h3>
              <li contentEditable={true} suppressContentEditableWarning={true}>
                <strong> Description : </strong>{elm.description}
              </li>
              {elm?.responsibility && (
                <li>
                  <strong>Responsibilities :</strong>

                  {elm?.responsibility.split(',').map((responsibility) => {
                    return (
                      <div className={styles.ul_container}>
                        {' '}
                        <ul>
                          <li
                            contentEditable={true}
                            suppressContentEditableWarning={true}
                            className={styles.responsibility_list}
                          >
                            <span>{responsibility}</span>
                          </li>
                        </ul>
                      </div>
                    );
                  })}
                </li>
              )}
              <li>
                <strong>Project Role : </strong> {elm.role}
              </li>
              {elm.teamSize && (
                <li>
                  <strong>Team Size : </strong> {elm.teamSize}
                </li>
              )}
              <li>
                <strong>Tech Tools : </strong> {elm.techTools}
              </li>
              {elm.url && (
                <li>
                  <strong>Url : </strong> {elm.url}
                </li>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
