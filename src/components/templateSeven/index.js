import React, { useContext } from 'react';
import { dataContext } from '../../context/dataContext';
import { Header } from './Header';
import { Subtitle } from './Subtitle';
import { Title } from './Title';
import { FiUsers } from 'react-icons/fi';
import { HiOutlineMail } from 'react-icons/hi';
import { RiMapPinLine } from 'react-icons/ri';
import { BsTelephone } from 'react-icons/bs';
import styles from './styles.module.css';

export const Tempseven = () => {
  const { data } = useContext(dataContext);
  return (
    <div className={styles.conatiner}>
      <Header {...data} />

      {data.contactUs && (
        <div className='contact-sec'>
          <div className='contact-details'>
            {data.contactUs[0].companyName && (
              <p>
                <strong>
                  <FiUsers />
                </strong>
                {data.contactUs[0].companyName}
              </p>
            )}
            {data.contactUs[0].email && (
              <p>
                <strong>
                  <HiOutlineMail />
                </strong>
                {data.contactUs[0].email}
              </p>
            )}
            {data.contactUs[0].mobilenumber && (
              <p>
                <strong>
                  <BsTelephone />
                </strong>
                {data.contactUs[0].mobilenumber}
              </p>
            )}
            {data.contactUs[0].location && (
              <p>
                <strong>
                  <RiMapPinLine />
                </strong>
                {data.contactUs[0].location}
              </p>
            )}
          </div>
        </div>
      )}
      <Title title={'Summary'} />
      <Subtitle subTitle={data.summary} />

   {/* Education   */}
      {data.education[0].qualification && (
        <div>
          <Title title={'Education'} />
          <div>
            {data?.education?.map((elm, i) => (
              <div key={i} className={styles.edu}>
                <p> {elm.qualification}</p>
                <p>{elm.fromWhere}</p>
                <p>{elm.when}</p>
              </div>
            ))}
          </div>
        </div>
      )}

      {/* work experience  */}
      {data.workExprience[0].companyname && (
        <div>
          <Title title={'Work Experience'} />
          <div>
            {data?.workExprience?.map((elm, i) => (
              <div key={i} className={styles.work}>
                <p> {elm.companyname}</p>
                <p>{elm.date}</p>
              </div>
            ))}
          </div>
        </div>
      )}

{/* Skills */}
      <Title title={'Skills'} />

      <div>
        {Object.keys(data.skills).map((skillName, i) => {
          if (data.skills[skillName]) {
            return (
              <li
                key={i}
                contentEditable={true}
                suppressContentEditableWarning={true}
                className={styles.subtitle}
              >
                <strong>{skillName} :</strong>
                <span>{data?.skills[skillName]}</span>
              </li>
            );
          } else {
            return null;
          }
        })}
        {data?.tools && (
          <li
            contentEditable={true}
            suppressContentEditableWarning={true}
            className={styles.subtitle}
          >
            <strong>Project Management tools :</strong>
            <span>{data.tools}</span>
          </li>
        )}
      </div>
      
{/* Professional Experience */}
      <Title title={'Professional Experience'} />
      <div>
        {data?.projects?.map((elm, i) => (
          <div key={i} className={styles.project}>
            <h3>
              <strong>PROJECT {i + 1}</strong> ({elm.projectName})
            </h3>
            <li contentEditable={true} suppressContentEditableWarning={true}>
              <strong> Description : </strong> {elm.description}
            </li>
            {elm?.responsibility && (
              <li>
                <strong>Responsibilities :</strong>

                {elm?.responsibility.split(',').map((responsibility) => {
                  return (
                    <ul className={styles.responsibility}>
                      <li
                        contentEditable={true}
                        suppressContentEditableWarning={true}
                        className={styles.responsibility_list}
                      >
                        <span>{responsibility}</span>
                      </li>
                    </ul>
                  );
                })}
              </li>
            )}
            <li>
              <strong>Project Role : </strong> {elm.role}
            </li>
            {elm.teamSize && (
              <li>
                <strong>Team Size : </strong> {elm.teamSize}
              </li>
            )}
            <li>
              <strong>Tech Tools : </strong> {elm.techTools}
            </li>
            {elm.url && (
              <li>
                <strong>Url : </strong> {elm.url}
              </li>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
