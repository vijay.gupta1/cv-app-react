import React, { useContext, useEffect } from 'react';
import styles from '../stylesheet/users.module.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { BASE_URL } from '../constant';
// import { Loader } from "./Loader";
import { dataContext } from '../context/dataContext';
import { useNavigate } from 'react-router-dom';
import { RiDeleteBin3Fill, RiFileEditFill } from 'react-icons/ri';
import { FaEye } from 'react-icons/fa';
import { Button } from 'technogetic-iron-smart-ui';

export default function Resumes() {
  const { user, allCv, setAllCv, setData } = useContext(dataContext);
  const navegate = useNavigate();
  const header = ['Name', 'Post', 'Preview', 'Edit', 'Delete'];
  const config = {
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('token')
    }
  };

  async function fetchData() {
    try {
      const res = await axios.get(`${BASE_URL}api/getpdf/${user._id}`, config);
      if (res.status !== 200) {
        throw new Error('API response was not successful');
      }
      setAllCv(res.data.data);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = ({ _id }) => {
    let url = `${BASE_URL}api/deletPdf/${_id}`;
    axios
      .delete(url, config)
      .then((res) => {
        if (res.status !== 200) {
          throw new Error('API response was not successful');
        }
        fetchData();
        alert('Resume Delete Successfully');
      })
      .catch((err) => console.error(err));
  };

  const handleRow = (obj) => {
    let activeData = allCv.filter((x) => x._id === obj._id);
    setData(activeData[0]);
    navegate('/template');
  };

  const handleEdit = (obj) => {
    let activeData = allCv.filter((x) => x._id === obj._id);
    setData(activeData[0]);
    navegate('/updateCv');
  };

  // if (!allCv) {
  //   return <Loader />;
  // }
  return (
    <div className={styles.container}>
      <div className={styles.main}>
        <div className={styles.heading}>
          <h1>All Resumes</h1>
          <Link to={'/createCV'}>
            <Button
            //  className={styles.btn}
             >Create CV</Button>
          </Link>
        </div>

        <div className={styles.tableBox}>
          <table className={styles.table}>
            <thead className={styles.tableheader}>
              <tr className={styles.tablerow}>
                {header.map((elm, i) => (
                  <th id={styles.tablecolumn} key={i}>
                    {elm}
                  </th>
                ))}
              </tr>
            </thead>
            {allCv?.map((obj, i) => (
              <tbody key={i}>
                <tr className={styles.tablerow}>
                  <td id={styles.tablecolumn} width={'200px'}>
                    {obj.name}
                  </td>
                  <td id={styles.tablecolumn} width={'250px'}>
                    {obj.post}
                  </td>
                  <td
                    id={styles.tablecolumn}
                    onClick={() => handleRow(obj)}
                    width={'100px'}
                    className='viewCv'
                  >
                    <FaEye />
                  </td>
                  <td
                    id={styles.tablecolumn}
                    onClick={() => handleEdit(obj)}
                    width={'100px'}
                    className='deleteCv'
                  >
                    <RiFileEditFill />
                  </td>
                  <td
                    id={styles.tablecolumn}
                    onClick={() => handleDelete(obj)}
                    width={'100px'}
                    className='deleteCv'
                  >
                    <RiDeleteBin3Fill />
                  </td>
                </tr>
              </tbody>
            ))}
          </table>
        </div>
      </div>
    </div>
  );
}
