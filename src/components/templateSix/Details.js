import React from 'react';
import { FiUsers } from 'react-icons/fi';
import { HiOutlineMail } from 'react-icons/hi';
import { RiMapPinLine } from 'react-icons/ri';
import { BsTelephone } from 'react-icons/bs';
import { Title } from './Title';
import styles from './styles.module.css';

export const Details = ({ isLogo, contactUs, education, projects }) => {
  // console.log(name,post, summary, education, projects);
  return (
    <>
      <div className={styles.right}>

        {/* <div className='logo-container'>
        {isLogo && (
          <div className='logo-img'>
            <img width={126} height={53} src='logoTG.png' alt='logo' />
          </div>
        )}
      </div> */}
        <div className='flex'>
          <div>
            {/* <div className='details-sec'>
              <Title title={'Summary'} />
              <p contentEditable='true' suppressContentEditableWarning={true} className='textDark '>
                {summary}
              </p>
            </div> */}
            {education && (
              <div className='details-sec'>
                {education[0].qualification && <Title title={'Education'} />}
                {education?.map((e, i) => (
                  <div key={i} className='education-Section'>
                    {e.qualification && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className='textDark'
                      >
                        {e.qualification}
                      </p>
                    )}
                    {e.fromWhere && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className='textDark'
                      >
                        {e.fromWhere}
                      </p>
                    )}
                    {e.when && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className='textDark'
                      >
                        {e.when}
                      </p>
                    )}
                  </div>
                ))}
                <br />
              </div>
            )}
            <div className='details-sec'>
              <Title title={'Projects'} />
              {projects?.map((e, i) => (
                <div key={i} className='project-Section'>
                  {e.projectName && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark projectTitle'
                    >
                      <span>Project Name :</span>
                      {e.projectName}
                    </p>
                  )}
                  {e.description && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark'
                    >
                      <strong>Description </strong>
                      {e.description}
                    </p>
                  )}
                  {e.role && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark'
                    >
                      <strong>Role </strong>
                      {e.role}
                    </p>
                  )}
                  {e.techTools && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark'
                    >
                      <strong>Tech Tools </strong>
                      {e.techTools}
                    </p>
                  )}
                  {e.teamSize && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark'
                    >
                      <strong>Team Size </strong>
                      {e.teamSize}
                    </p>
                  )}
                  {e.url && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className='textDark'
                    >
                      <strong>Url </strong>
                      {e.url}
                    </p>
                  )}
                </div>
              ))}
            </div>
            <div className='details-sec'>
            {contactUs && (
          <div className='contact-sec'>
            {contactUs[0].companyName && <Title title={'Contact Us'} />}
            <div className='contact-details'>
              {contactUs[0].companyName && (
                <p>
                  <strong>
                    <FiUsers />
                  </strong>
                  {contactUs[0].companyName}
                </p>
              )}
              {contactUs[0].email && (
                <p>
                  <strong>
                    <HiOutlineMail />
                  </strong>
                  {contactUs[0].email}
                </p>
              )}
              {contactUs[0].mobilenumber && (
                <p>
                  <strong>
                    <BsTelephone />
                  </strong>
                  {contactUs[0].mobilenumber}
                </p>
              )}
              {contactUs[0].location && (
                <p>
                  <strong>
                    <RiMapPinLine />
                  </strong>
                  {contactUs[0].location}
                </p>
              )}
            </div>
          </div>
        )}
            </div>

          </div>
        </div>
      </div>
    </>
  );
};
