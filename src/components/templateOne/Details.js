import React from 'react';
import { Header } from './Header';
import { GoPrimitiveDot } from 'react-icons/go';
import { Title } from './Title';
import styles from './styles.module.css';

export const Details = ({ name, post, summary, education, projects }) => {
  return (
    <>
      <div className={styles.left_part}>
        <div className={styles.flex_header}>
          <Header name={name} post={post} />
        </div>
        <div className={styles.flex}>
          <div className={styles.sideborder}></div>
          <div>
            <div className={styles.details_sec}>
              <div className={styles.circle} >
                <GoPrimitiveDot style={{ fontSize: '2rem', color: 'lightgrey' }} />
              </div>
              <Title title={'Summary'} />
              <p contentEditable='true' suppressContentEditableWarning={true} className={styles.textDark }>
                {summary}
              </p>
            </div>
            {education && (
              <div className={styles.details_sec}>
                {education[0].qualification && (
                  <div className={styles.circle}>
                    <GoPrimitiveDot style={{ fontSize: '2rem', color: 'lightgrey' }} />
                  </div>
                )}
                {education[0].qualification && <Title title={'Education'} />}
                {education?.map((e, i) => (
                  <div key={i} className={styles.education_Section}>
                    {e.qualification && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className={styles.textDark }
                      >
                        {e.qualification}
                      </p>
                    )}
                    {e.fromWhere && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className={styles.textDark }
                      >
                        {e.fromWhere}
                      </p>
                    )}
                    {e.when && (
                      <p
                        contentEditable='true'
                        suppressContentEditableWarning={true}
                        className={styles.textDark }
                      >
                        {e.when}
                      </p>
                    )}
                  </div>
                ))}
                <br />
              </div>
            )}
            <div className={styles.details_sec}>
              <div className={styles.circle}>
                <GoPrimitiveDot style={{ fontSize: '2rem', color: 'lightgrey' }} />
              </div>
              <Title title={'Projects'} />
              {projects?.map((e, i) => (
                <div key={i} className={styles.project_Section}>
                  {e.projectName && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
             className={styles.textDarkprojectTitle}
                      
                    >
                      <strong>Project Name :</strong>
                      {e.projectName}
                    </p>
                  )}
                  {e.description && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className={styles.textDark }

                    >
                      <strong>Description </strong>
                      {e.description}
                    </p>
                  )}
                  {e.role && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className={styles.textDark }

                    >
                      <strong>Role </strong>
                      {e.role}
                    </p>
                  )}
                  {e.techTools && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className={styles.textDark }

                    >
                      <strong>Tech Tools </strong>
                      {e.techTools}
                    </p>
                  )}
                  {e.teamSize && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className={styles.textDark }

                    >
                      <strong>Team Size </strong>
                      {e.teamSize}
                    </p>
                  )}
                  {e.url && (
                    <p
                      contentEditable='true'
                      suppressContentEditableWarning={true}
                      className={styles.textDark }

                    >
                      <strong>Url </strong>
                      {e.url}
                    </p>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
