import React from 'react';
import { FiUsers } from 'react-icons/fi';
import { HiOutlineMail } from 'react-icons/hi';
import { RiMapPinLine } from 'react-icons/ri';
import { BsTelephone } from 'react-icons/bs';
import { Title } from './Title';
import styles from './styles.module.css';

export const Technology = ({ isLogo, contactUs, skills, tools, langaugeSkills }) => {
  const allSkills = Object.keys(skills).reduce((acc, cv) => {
    acc = [...acc, ...skills[cv].split(',')];
    return acc;
  }, []);
  return (
    <div className={styles.right_part}>
      <div className={styles.logo_container}>
        {isLogo && (
          <div className={styles.logo_img}>
            <img width={150} height={53} src='logoTG.png' alt='logo' />
          </div>
        )}
      </div>
      <div className={styles.content_container}>
        {' '}
        {/* skills */}
        <div>
          <Title title={'Skills'} />
          <ul>
                 {allSkills.map((skills, i) =>  {
              if (skills.length > 2) {
                return (
                  <li key={i} className={styles.textDark }>
                    {skills}
                  </li>
                );

                return null;
              }
            })}
          </ul>
        </div>
        {/* Tools */}
        <div>
          <Title title={'Tools'} />
          <ul>
            {tools &&
              tools
                .split(',')
                .filter((x) => x.length >= 2)
                .map((skills, i) => (
                  <li key={i} className={styles.textDark }>
                    {skills}
                  </li>
                ))}
          </ul>
        </div>
        {/* language */}
        <div className={styles.language_sec}>
          <Title title={'Language Skills'} />
          <div className={styles.language_list}>
            {langaugeSkills &&
              langaugeSkills
                .split(',')
                .filter((x) => x.length >= 2)
                .map((skills, i) => (
                  <strong key={i} className={styles.language}>
                    - {skills}
                  </strong>
                ))}
          </div>
        </div>
        {/* contact  */}
        {contactUs && (
          <div className={styles.contact_sec}>
            {contactUs[0].companyName && <Title title={'Contact Us'} />}
            <div className={styles.contact_details}>
              {contactUs[0].companyName && (
                <p>
                  <strong>
                    <FiUsers />
                  </strong>
                  {contactUs[0].companyName}
                </p>
              )}
              {contactUs[0].email && (
                <p>
                  <strong>
                    <HiOutlineMail />
                  </strong>
                  {contactUs[0].email}
                </p>
              )}
              {contactUs[0].mobilenumber && (
                <p>
                  <strong>
                    <BsTelephone />
                  </strong>
                  {contactUs[0].mobilenumber}
                </p>
              )}
              {contactUs[0].location && (
                <p>
                  <strong>
                    <RiMapPinLine />
                  </strong>
                  {contactUs[0].location}
                </p>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
