import React from 'react';
import { FiUsers } from 'react-icons/fi';
import { HiOutlineMail } from 'react-icons/hi';
import { RiMapPinLine } from 'react-icons/ri';
import { BsTelephone } from 'react-icons/bs';
import { Title } from './Title';
import styles from './styles.module.css';

export const Technology = ({ isLogo, contactUs, skills, tools, langaugeSkills }) => {
  const allSkills = Object.keys(skills).reduce((acc, cv) => {
    acc = [...acc, ...skills[cv].split(',')];
    return acc;
  }, []);
  return (
    <div className={styles.left}>
      <div className='logo-container'>
        {isLogo && (
          <div className='logo-img'>
            <img width={126} height={53} src='logoTG.png' alt='logo' />
          </div>
        )}
      </div>
      <div className='content-container'>
        {' '}
        {/* skills */}
        <div>
          <Title title={'Skills'} />
          <ul>
            {allSkills.map((skills, i) => {
              if (skills.length > 2) {
                return (
                  <li key={i} className='textDark'>
                    {skills}
                  </li>
                );

                return null;
              }
            })}
          </ul>
        </div>
        {/* Tools */}
        <div>
          <Title title={'Tools'} />
          <ul>
            {tools &&
              tools
                .split(',')
                .filter((x) => x.length >= 2)
                .map((skills, i) => (
                  <li key={i} className='textDark'>
                    {skills}
                  </li>
                ))}
          </ul>
        </div>
        {/* language */}
        <div className='language-sec'>
          <Title title={'Language Skills'} />
          <div className='language-list'>
            {langaugeSkills &&
              langaugeSkills
                .split(',')
                .filter((x) => x.length > 1)
                .map((skillName, i) => (
                  <strong key={i} className='language'>
                    - {skillName}
                  </strong>
                ))}
          </div>
        </div>
        {/* contact  */}
        {contactUs && (
          <div className='contact-sec'>
            {contactUs[0].companyName && <Title title={'Contact Us'} />}
            <div className='contact-details'>
              {contactUs[0].companyName && (
                <p>
                  <strong>
                    <FiUsers />
                  </strong>
                  {contactUs[0].companyName}
                </p>
              )}
              {contactUs[0].email && (
                <p>
                  <strong>
                    <HiOutlineMail />
                  </strong>
                  {contactUs[0].email}
                </p>
              )}
              {contactUs[0].mobilenumber && (
                <p>
                  <strong>
                    <BsTelephone />
                  </strong>
                  {contactUs[0].mobilenumber}
                </p>
              )}
              {contactUs[0].location && (
                <p>
                  <strong>
                    <RiMapPinLine />
                  </strong>
                  {contactUs[0].location}
                </p>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
